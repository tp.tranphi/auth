package api

import (
	"encoding/json"
	"gitlab.ghn.vn/common-projects/go-sdk/sdk"
	"gitlab.ghn.vn/hub-system/iam/model"
)

// AppGet process GET method on /iam/app
// can query by id like /iam/app?id=<id>
// or get list like /iam/app?q={"key":"value"}&offset=100&limit=10&reverse=true
func AppGet(req sdk.APIRequest, resp sdk.APIResponder) error {
	// check if get by id
	idStr := req.GetParam("id")
	if idStr != "" {
		return resp.Respond(model.AppDB.G(idStr, &model.App{}))
	}

	// if get list
	var qStr = req.GetParam("q")
	var q model.App
	if qStr != "" {
		json.Unmarshal([]byte(qStr), &q)
	}
	var offset = sdk.ParseInt(req.GetParam("offset"), 0)
	var limit = sdk.ParseInt(req.GetParam("limit"), 10)
	var reverse = req.GetParam("reverse") == "true"
	
	var result = make([]model.App, limit)

	// setup query
	query, err := model.AppDB.Q(q, offset, limit, reverse)
	if err == nil {
		err = query.All(&result)
	}
	response := model.AppDB.R(result, err)

	var getTotal = req.GetParam("getTotal") == "true"
	if getTotal {
		total, err := model.AppDB.Count(q)
		if err != nil {
			response.Total = 0
		} else {
			response.Total = int64(total)
		}
	}

	return resp.Respond(response)

}

// AppPut process PUT method on /iam/app
func AppPut(req sdk.APIRequest, resp sdk.APIResponder) error {
	// parse input
	var perm model.App
	err := req.GetContent(&perm)
	if err != nil {
		return resp.Respond(&sdk.APIResponse{
			Status:  sdk.APIStatus.Invalid,
			Message: "Invalid input",
		})
	}

	// do action
	return resp.Respond(model.AppDB.U(perm.ID.Hex(), perm))
}

// AppPost process POST method on /iam/app
func AppPost(req sdk.APIRequest, resp sdk.APIResponder) error {
	// parse input
	var perm model.App
	err := req.GetContent(&perm)
	if err != nil {
		return resp.Respond(&sdk.APIResponse{
			Status:  sdk.APIStatus.Invalid,
			Message: "Invalid input",
		})
	}

	// do action
	return resp.Respond(model.AppDB.I(&perm))
}

// AppDelete process DELETE method on /iam/app?id={id}
func AppDelete(req sdk.APIRequest, resp sdk.APIResponder) error {
	idStr := req.GetParam("id")
	return resp.Respond(model.AppDB.D(idStr))
}
