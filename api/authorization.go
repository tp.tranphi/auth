package api

import (
	"fmt"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"gitlab.ghn.vn/common-projects/go-sdk/sdk"
	"gitlab.ghn.vn/hub-system/iam/model"
)

// AuthorizationInput input data of the API
type AuthorizationInput struct {
	PartnerCode string `json:"partnerCode"`
	OrgCode     string `json:"orgCode"`
	Token       string `json:"token"`
	Method      string `json:"method"`
	Path        string `json:"path"`
}

func onAuthorizationSucess(user *model.User, partner *model.SystemPartner, orgCode string, resp sdk.APIResponder) error {
	return resp.Respond(&sdk.APIResponse{
		Status:  sdk.APIStatus.Ok,
		Message: "Allow to access.",
		Data: []model.ActionSource{
			model.ActionSource{
				User:    user,
				Partner: partner,
				OrgCode: orgCode,
			},
		},
	})
}

// AuthorizationPost verify if one user can access
func AuthorizationPost(req sdk.APIRequest, resp sdk.APIResponder) error {
	var input AuthorizationInput
	err := req.GetContent(&input)
	if err != nil {
		resp.Respond(&sdk.APIResponse{
			Status:  sdk.APIStatus.Invalid,
			Message: "Can not parse input data.",
		})
		return nil
	}
	return Authorize(input, resp)
}

//Authorize authorize via input
func Authorize(input AuthorizationInput, resp sdk.APIResponder) error {
	var partnerRole *model.Role
	var userID string
	var user *model.User

	partner := model.SystemPartnerCache[input.PartnerCode]
	if partner == nil {
		return resp.Respond(&sdk.APIResponse{
			Status:  sdk.APIStatus.Forbidden,
			Message: "Invalid partner.",
		})
	}

	// get token data
	token, err := jwt.Parse(input.Token, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(partner.Secret), nil
	})

	if err != nil {
		return resp.Respond(&sdk.APIResponse{
			Status:  sdk.APIStatus.Invalid,
			Message: "Access token is invalid.",
		})
	}

	// validate expired time
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		expired, err := time.Parse(time.RFC3339, claims["expired"].(string))

		if err != nil {
			return resp.Respond(&sdk.APIResponse{
				Status:  sdk.APIStatus.Error,
				Message: "Parse time error: " + err.Error(),
			})
		}

		if expired.Before(time.Now()) {
			return resp.Respond(&sdk.APIResponse{
				Status:  sdk.APIStatus.Invalid,
				Message: "Access token was expired.",
			})
		}

		if claims["userId"] != nil {
			userID = claims["userId"].(string)
		}
	}

	// get partner role
	partnerRole = model.GetCachedRole(partner.AppCode, partner.RoleCode)

	// if role not found
	if partnerRole == nil {
		return resp.Respond(&sdk.APIResponse{
			Status:  sdk.APIStatus.NotFound,
			Message: "Role " + partner.RoleCode + " wasn't found.",
		})
	}

	// verify permission
	var allow = false
	var skipUserLogin = false
	for _, pCode := range partnerRole.Permissions {
		permission := model.GetCachedPermission(partner.AppCode, pCode)
		if (permission != nil && (permission.Method == "ALL" || permission.Method == input.Method)) && strings.HasPrefix(input.Path, permission.Path) {
			allow = true
			skipUserLogin = permission.CanSkipLogin
			break
		}
	}

	// if partner don't have permission
	if !allow {
		return resp.Respond(&sdk.APIResponse{
			Status:  sdk.APIStatus.Forbidden,
			Message: "This partner don't have permission to do this action.",
		})
	}

	if userID == "" {
		if skipUserLogin {
			return onAuthorizationSucess(nil, partner, input.OrgCode, resp)
		}
		return resp.Respond(&sdk.APIResponse{
			Status:  sdk.APIStatus.Forbidden,
			Message: "Require login to do this action. .",
		})
	}

	// get user
	u, ok := model.UserCache.Get(userID)
	if !ok {
		uResponse := model.UserDB.G(userID, &user)
		if uResponse.Status != sdk.APIStatus.Ok {
			return resp.Respond(uResponse)
		}

		model.UserCache.Put(userID, user)
	} else {
		user = u.(*model.User)
	}

	// accept if is system admin
	if user.IsSystemAdmin {
		return onAuthorizationSucess(user, partner, input.OrgCode, resp)
	}

	// get user roles
	var role *model.UserRole
	var roleQuery = model.UserRole{
		AppCode: partner.AppCode,
		OrgCode: input.OrgCode,
		UserID:  user.UniqueID,
		Status:  "ACTIVE",
	}

	// check cache
	key := partner.AppCode + "/" + input.OrgCode + "/" + user.UniqueID
	r, ok := model.UserRoleCache.Get(key)
	if !ok {
		q, _ := model.UserRoleDB.Q(roleQuery, 0, 0, false)
		err = q.One(&role)
		if err != nil {
			return resp.Respond(&sdk.APIResponse{
				Status:  sdk.APIStatus.Forbidden,
				Message: "This user don't have any role.",
			})
		}

		model.UserRoleCache.Put(key, role)
	} else {
		role = r.(*model.UserRole)
	}

	for _, roleCode := range role.RoleCodes {
		userRole := model.GetCachedRole(partner.AppCode, roleCode)
		if userRole != nil {
			for _, pCode := range userRole.Permissions {
				userPermission := model.GetCachedPermission(partner.AppCode, pCode)
				if userPermission != nil {
					if (userPermission.Method == "ALL" || userPermission.Method == input.Method) && strings.HasPrefix(input.Path, userPermission.Path) {

						// update last user active
						var now = time.Now()
						if role.LastActive.Add(30 * time.Second).Before(now) {
							go updateLastActive(role.ID.Hex())
						}

						return onAuthorizationSucess(user, partner, input.OrgCode, resp)
					}
				}
			}
		}
	}

	return resp.Respond(&sdk.APIResponse{
		Status:  sdk.APIStatus.Forbidden,
		Message: "This user don't have permission to do this action.",
	})
}

func updateLastActive(id string) {
	model.UserRoleDB.U(id, model.UserRole{
		LastActive: time.Now(),
	})
}
