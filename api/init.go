package api

import (
	"encoding/json"
	"strings"

	jwt "github.com/dgrijalva/jwt-go"
	"gitlab.ghn.vn/common-projects/go-sdk/sdk/thriftapi"

	"gitlab.ghn.vn/common-projects/go-sdk/sdk"
	"gitlab.ghn.vn/hub-system/iam/model"
)

func init() {

}

// GetActionSource ...
func GetActionSource(req sdk.APIRequest) *model.ActionSource {
	var source model.ActionSource
	sourceAttr := req.GetAttribute("X-Source")
	if sourceAttr != nil {
		source = sourceAttr.(model.ActionSource)
		return &source
	}
	sourceStr := req.GetHeader("X-Source")
	if sourceStr == "" {
		return nil
	}

	err := json.Unmarshal([]byte(sourceStr), &source)

	if err != nil {
		return nil
	}

	return &source
}

//IAMAuthorization ...
type IAMAuthorization struct {
	Result *sdk.APIResponse
}

//Respond ...
func (i *IAMAuthorization) Respond(r *sdk.APIResponse) error {
	i.Result = r
	return nil
}

//GetThriftResponse ...
func (i *IAMAuthorization) GetThriftResponse() *thriftapi.APIResponse {
	return nil
}

//PreRequest before handler
func PreRequest(req sdk.APIRequest, resp sdk.APIResponder) error {
	path := req.GetPath()
	if path == "/iam/v1/system-partner/access-token" || path == "/iam/v1/authorization" {
		return nil
	}

	r2 := &IAMAuthorization{}
	authStr := req.GetHeader("Authorization")
	if authStr == "" {
		resp.Respond(&sdk.APIResponse{
			Status:  sdk.APIStatus.Forbidden,
			Message: "Authorization header is required.",
		})

		return &sdk.Error{Type: "AUTHORIZATION_MISSING"}
	}

	// tok bearer
	arr := strings.Split(authStr, " ")
	if len(arr) != 2 {
		resp.Respond(&sdk.APIResponse{
			Status:  sdk.APIStatus.Forbidden,
			Message: "Authorization header is invalid.",
		})

		return &sdk.Error{Type: "AUTHORIZATION_INVALID"}
	}

	// parse jwt format, without validate encryption
	authStr = arr[1]
	token, _ := jwt.Parse(authStr, nil)
	if token == nil || token.Claims == nil {
		resp.Respond(&sdk.APIResponse{
			Status:  sdk.APIStatus.Forbidden,
			Message: "Authorization format is invalid.",
		})

		return &sdk.Error{Type: "AUTHORIZATION_FORMAT_INVALID"}
	}
	claims := token.Claims.(jwt.MapClaims)

	// setup auth input
	input := &AuthorizationInput{
		PartnerCode: claims["partnerCode"].(string),
		OrgCode:     claims["orgCode"].(string),
		Token:       authStr,
		Method:      req.GetMethod().Value,
		Path:        req.GetPath(),
	}
	Authorize(*input, r2)

	if r2.Result == nil {
		resp.Respond(&sdk.APIResponse{
			Status:  sdk.APIStatus.Forbidden,
			Message: "Error occurs when authorize.",
		})
		return &sdk.Error{Type: "Authorization error."}
	}

	if r2.Result.Status != sdk.APIStatus.Ok {
		resp.Respond(r2.Result)
		return &sdk.Error{
			Type:    "Authorization failed.",
			Message: r2.Result.Message,
		}
	}

	req.SetAttribute("X-Source", r2.Result.Data.([]model.ActionSource)[0])

	return nil
}
