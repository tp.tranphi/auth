package api

import (
	"github.com/globalsign/mgo/bson"
	"gitlab.ghn.vn/common-projects/go-sdk/sdk"
	"gitlab.ghn.vn/hub-system/iam/model"
)

// OrganizationGet process GET method on /iam/organization
func OrganizationGet(req sdk.APIRequest, resp sdk.APIResponder) error {
	// check if get by id
	idStr := req.GetParam("id")
	if idStr != "" {
		return resp.Respond(model.OrganizationDB.G(idStr, &model.Organization{}))
	}

	// if get list
	var offset = sdk.ParseInt(req.GetParam("offset"), 0)
	var limit = sdk.ParseInt(req.GetParam("limit"), 10)
	var reverse = req.GetParam("reverse") == "true"
	var result = make([]model.Organization, limit)

	// setup query
	query, err := model.OrganizationDB.Q(bson.M{}, offset, limit, reverse)
	if err == nil {
		err = query.All(&result)
	}
	response := model.OrganizationDB.R(result, err)

	// if want to get total
	var getTotal = req.GetParam("getTotal") == "true"
	if getTotal {
		total, err := query.Count()
		if err != nil {
			response.Total = int64(total)
		} else {
			response.Total = 0
		}
	}

	return resp.Respond(response)

}

// OrganizationPut process PUT method on /iam/organization
func OrganizationPut(req sdk.APIRequest, resp sdk.APIResponder) error {
	// parse input
	var org model.Organization
	err := req.GetContent(&org)
	if err != nil {
		resp.Respond(&sdk.APIResponse{
			Status:  sdk.APIStatus.Invalid,
			Message: "Invalid input",
		})
		return err
	}

	// do action
	return resp.Respond(model.OrganizationDB.U(org.ID.Hex(), org))
}

// OrganizationPost process POST method on /iam/organization
func OrganizationPost(req sdk.APIRequest, resp sdk.APIResponder) error {
	// parse input
	var org model.Organization
	err := req.GetContent(&org)
	if err != nil {
		resp.Respond(&sdk.APIResponse{
			Status:  sdk.APIStatus.Invalid,
			Message: "Invalid input",
		})
		return err
	}

	// do action
	return resp.Respond(model.OrganizationDB.I(&org))
}
