package api

import (
	"gitlab.ghn.vn/common-projects/go-sdk/sdk"
	"gitlab.ghn.vn/hub-system/iam/model"
	"encoding/json"
)

// PermissionGet process GET method on /iam/permission
// can query by id like /iam/permission?id=<id>
// or get list like /iam/permission?q={"key":"value"}&offset=100&limit=10&reverse=true
func PermissionGet(req sdk.APIRequest, resp sdk.APIResponder) error {
	// check if get by id
	idStr := req.GetParam("id")
	if idStr != "" {
		return resp.Respond(model.PermissionDB.G(idStr, &model.Permission{}))
	}
	
	// if get list
	var qStr = req.GetParam("q")
	var q model.Permission
	if qStr != "" {
		err := json.Unmarshal([]byte(qStr), &q)
		if err != nil {
			q = model.Permission{}
		}
	}
	var offset = sdk.ParseInt(req.GetParam("offset"), 0)
	var limit = sdk.ParseInt(req.GetParam("limit"), 10)
	var reverse = req.GetParam("reverse") == "true"
	var result = make([]model.Permission, limit)

	// setup query
	query, err := model.PermissionDB.Q(q, offset, limit, reverse)
	if err == nil {
		err = query.All(&result)
	}
	response := model.PermissionDB.R(result, err)

	// if want to get total
	var getTotal = req.GetParam("getTotal") == "true"
	if getTotal {
		total, err := model.PermissionDB.Count(q)
		if err != nil {
			response.Total = int64(total)
		} else {
			response.Total = 0
		}
	}


	return resp.Respond(response)

}

// PermissionPut process PUT method on /iam/permission
func PermissionPut(req sdk.APIRequest, resp sdk.APIResponder) error {
	// parse input
	var perm model.Permission
	err := req.GetContent(&perm)
	if err != nil {
		return resp.Respond(&sdk.APIResponse{
			Status:  sdk.APIStatus.Invalid,
			Message: "Invalid input",
		})
	}

	// do action
	return resp.Respond(model.PermissionDB.U(perm.ID.Hex(), perm))
}

// PermissionPost process POST method on /iam/permission
func PermissionPost(req sdk.APIRequest, resp sdk.APIResponder) error {
	// parse input
	var perm model.Permission
	err := req.GetContent(&perm)
	if err != nil {
		return resp.Respond(&sdk.APIResponse{
			Status:  sdk.APIStatus.Invalid,
			Message: "Invalid input",
		})
	}

	// do action
	return resp.Respond(model.PermissionDB.I(&perm))
}

// PermissionDelete process DELETE method on /iam/permission?id={id}
func PermissionDelete(req sdk.APIRequest, resp sdk.APIResponder) error {
	idStr := req.GetParam("id")
	return resp.Respond(model.PermissionDB.D(idStr))
}
