package api

import (
	"encoding/json"

	"gitlab.ghn.vn/common-projects/go-sdk/sdk"
	"gitlab.ghn.vn/hub-system/iam/model"
)

// RoleGet process GET method on /iam/v1/role
// can query by id like /iam/v1/role?id=<id>
// or get list like /iam/v1/role?q={"key":"value"}&offset=100&limit=10&reverse=true
func RoleGet(req sdk.APIRequest, resp sdk.APIResponder) error {
	// check if get by id
	idStr := req.GetParam("id")
	if idStr != "" {
		return resp.Respond(model.RoleDB.G(idStr, &model.Role{}))
	}

	// if get list
	var qStr = req.GetParam("q")
	var q model.Role
	if qStr != "" {
		err := json.Unmarshal([]byte(qStr), &q)
		if err != nil {
			q = model.Role{}
		}
	} else {
		q = model.Role{}
	}
	
	var offset = sdk.ParseInt(req.GetParam("offset"), 0)
	var limit = sdk.ParseInt(req.GetParam("limit"), 10)
	var reverse = req.GetParam("reverse") == "true"
	var result = make([]model.Role, limit)

	// setup query
	query, err := model.RoleDB.Q(q, offset, limit, reverse)
	if err == nil {
		err = query.All(&result)
	}
	response := model.RoleDB.R(result, err)
	return resp.Respond(response)

}

// RolePut process PUT method on /iam/v1/role
func RolePut(req sdk.APIRequest, resp sdk.APIResponder) error {
	// parse input
	var role model.Role
	err := req.GetContent(&role)
	if err != nil {
		return resp.Respond(&sdk.APIResponse{
			Status:  sdk.APIStatus.Invalid,
			Message: "Invalid input",
		})
	}

	// check type
	if role.Type != "SYSTEM" {
		role.Type = "USER"
	}

	// do action
	return resp.Respond(model.RoleDB.U(role.ID.Hex(), role))
}

// RolePost process POST method on /iam/v1/role
func RolePost(req sdk.APIRequest, resp sdk.APIResponder) error {
	// parse input
	var role model.Role
	err := req.GetContent(&role)
	if err != nil {
		return resp.Respond(&sdk.APIResponse{
			Status:  sdk.APIStatus.Invalid,
			Message: "Invalid input",
		})
	}

	// check type
	if role.Type != "SYSTEM" {
		role.Type = "USER"
	}

	// do action
	return resp.Respond(model.RoleDB.I(&role))
}

// RoleDelete process DELETE method on /iam/v1/role?id={id}
func RoleDelete(req sdk.APIRequest, resp sdk.APIResponder) error {
	idStr := req.GetParam("id")
	return resp.Respond(model.RoleDB.D(idStr))
}
