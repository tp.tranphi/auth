package api

import (
	"math/rand"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"gitlab.ghn.vn/common-projects/go-sdk/sdk"
	"gitlab.ghn.vn/hub-system/iam/client"
	"gitlab.ghn.vn/hub-system/iam/config"
)

type loginByT62Input struct {
	T62 string `json:"t62,omitempty"`
}

// SessionT62Post process POST method on /iam/v1/session/t62
func SessionT62Post(req sdk.APIRequest, resp sdk.APIResponder) error {
	var input loginByT62Input
	err := req.GetContent(&input)
	if err != nil {
		return resp.Respond(&sdk.APIResponse{
			Status:  sdk.APIStatus.Invalid,
			Message: "Invalid input: " + err.Error(),
		})
	}

	if input.T62 == "" {
		return resp.Respond(&sdk.APIResponse{
			Status:  sdk.APIStatus.Invalid,
			Message: "Invalid input: require T62 value",
		})
	}

	result := client.VerifyT62(input.T62)
	if result.Status != sdk.APIStatus.Ok {
		return resp.Respond(result)
	}

	ssoID := result.Data.([]string)[0]
	userResult := client.GetGHNUser(ssoID)
	if userResult.Status != sdk.APIStatus.Ok {
		return resp.Respond(userResult)
	}
	var source = GetActionSource(req)

	// generate token
	userID := userResult.Data.([]string)[0]
	expired := time.Now().Add(time.Duration(config.Config.Number["session-expired-hour"]) * time.Hour)
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"userId":      userID,
		"ssoId":       ssoID,
		"expired":     expired,
		"createdTime": time.Now(),
		"seed":        rand.Int(),
		"partnerCode": source.Partner.Code,
		"orgCode":     source.OrgCode,
		"appCode":     source.Partner.AppCode,
	})
	tokenStr, err := token.SignedString([]byte(source.Partner.Secret))

	if err != nil {
		return resp.Respond(&sdk.APIResponse{
			Status:  sdk.APIStatus.Error,
			Message: "Generate token error: " + err.Error(),
		})
	}

	return resp.Respond(&sdk.APIResponse{
		Status:  sdk.APIStatus.Ok,
		Message: tokenStr,
	})
}

// SessionExtendGet process POST method on /iam/v1/session/extended
func SessionExtendGet(req sdk.APIRequest, resp sdk.APIResponder) error {
	var source = GetActionSource(req)

	expired := time.Now().Add(time.Duration(config.Config.Number["session-expired-hour"]) * time.Hour)
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"userId":      source.User.ID,
		"ssoId":       source.User.UniqueID,
		"expired":     expired,
		"createdTime": time.Now(),
		"seed":        rand.Int(),
		"partnerCode": source.Partner.Code,
		"orgCode":     source.OrgCode,
		"appCode":     source.Partner.AppCode,
	})
	tokenStr, err := token.SignedString([]byte(source.Partner.Secret))

	if err != nil {
		resp.Respond(&sdk.APIResponse{
			Status:  sdk.APIStatus.Error,
			Message: "Generate token error: " + err.Error(),
		})
	}

	return resp.Respond(&sdk.APIResponse{
		Status:  sdk.APIStatus.Ok,
		Message: tokenStr,
	})
}
