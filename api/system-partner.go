package api

import (
	"encoding/json"
	"math/rand"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"gitlab.ghn.vn/common-projects/go-sdk/sdk"
	"gitlab.ghn.vn/hub-system/iam/config"
	"gitlab.ghn.vn/hub-system/iam/model"
)

// AccessTokenGet process GET method at /iam/v1/system-partner/access-token?partner=<partner-code>&public=<public-key>
func AccessTokenGet(req sdk.APIRequest, resp sdk.APIResponder) error {

	partnerCode := req.GetParam("partner")
	publicKey := req.GetParam("public")
	orgCode := req.GetParam("orgCode")
	appCode := req.GetParam("appCode")

	var partner = model.SystemPartnerCache[partnerCode]

	if partner == nil || partner.Public != publicKey || (appCode != "" && appCode != partner.AppCode) {
		resp.Respond(&sdk.APIResponse{
			Status:  sdk.APIStatus.Forbidden,
			Message: "Forbidden.",
		})
	}

	expired := time.Now().Add(time.Duration(config.Config.Number["session-expired-hour"]) * time.Hour)
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"expired":     expired,
		"createdTime": time.Now(),
		"seed":        rand.Int(),
		"partnerCode": partnerCode,
		"orgCode":     orgCode,
		"appCode":     partner.AppCode,
	})
	tokenStr, err := token.SignedString([]byte(partner.Secret))

	if err != nil {
		return resp.Respond(&sdk.APIResponse{
			Status:  sdk.APIStatus.Error,
			Message: "Generate error: " + err.Error(),
		})
	}

	return resp.Respond(&sdk.APIResponse{
		Status:  sdk.APIStatus.Ok,
		Message: tokenStr,
	})
}

//SystemPartnerGet process GET method on /iam/system-partner
// can query by id like /iam/system-partner?id=<id>
// or get list like /iam/system-partner?q={"key":"value"}&offset=100&limit=10&reverse=true
func SystemPartnerGet(req sdk.APIRequest, resp sdk.APIResponder) error {
	// check if get by id
	idStr := req.GetParam("id")
	if idStr != "" {
		return resp.Respond(model.SystemPartnerDB.G(idStr, &model.SystemPartner{}))
	}

	// if get list
	var qStr = req.GetParam("q")
	var q model.SystemPartner
	if qStr != "" {
		err := json.Unmarshal([]byte(qStr), &q)
		if err != nil {
			q = model.SystemPartner{}
		}
	}
	var offset = sdk.ParseInt(req.GetParam("offset"), 0)
	var limit = sdk.ParseInt(req.GetParam("limit"), 10)
	var reverse = req.GetParam("reverse") == "true"
	var result = []model.SystemPartner{}

	// setup query
	query, err := model.SystemPartnerDB.Q(q, offset, limit, reverse)
	if err == nil {
		err = query.All(&result)

		// clear Secret in result
		if err == nil {
			for _, partner := range result {
				partner.Secret = ""
			}
		}
	}
	response := model.SystemPartnerDB.R(result, err)

	// if want to get total
	var getTotal = req.GetParam("getTotal") == "true"
	if getTotal {
		total, err := query.Count()
		if err != nil {
			response.Total = int64(total)
		} else {
			response.Total = 0
		}
	}

	return resp.Respond(response)

}

var keyTemplate = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM0123456789"

// SystemPartnerPost process POST method on /iam/v1/system-partner
func SystemPartnerPost(req sdk.APIRequest, resp sdk.APIResponder) error {

	// parse input
	var partner model.SystemPartner
	err := req.GetContent(&partner)
	if err != nil {
		resp.Respond(&sdk.APIResponse{
			Status:  sdk.APIStatus.Invalid,
			Message: "Invalid input",
		})
		return err
	}

	if partner.AppCode == "" || partner.Name == "" || partner.Code == "" || partner.RoleCode == "" {
		return resp.Respond(&sdk.APIResponse{
			Status:  sdk.APIStatus.Invalid,
			Message: "Invalid input",
		})
	}

	var source = GetActionSource(req)
	var role = model.GetCachedRole(source.Partner.AppCode, partner.RoleCode)
	if role == nil {
		return resp.Respond(&sdk.APIResponse{
			Status:  sdk.APIStatus.NotFound,
			Message: "Role #" + partner.RoleCode + " is not found",
		})
	}

	// gen key
	partner.ID = ""
	partner.Public = ""
	partner.Secret = ""
	var n = len(keyTemplate)
	for i := 0; i < 32; i++ {
		partner.Public += string(keyTemplate[rand.Intn(n)])
		partner.Secret += string(keyTemplate[rand.Intn(n)])
	}

	// do action
	return resp.Respond(model.SystemPartnerDB.I(&partner))
}
