package api

import (
	"gitlab.ghn.vn/common-projects/go-sdk/sdk"
	"gitlab.ghn.vn/hub-system/iam/model"
	"encoding/json"
)

//UserRoleGet process GET method on /iam/v1/user
func UserRoleGet(req sdk.APIRequest, resp sdk.APIResponder) error {

	// check if get by id
	idStr := req.GetParam("id")
	if idStr != "" {
		return resp.Respond(model.UserRoleDB.G(idStr, &model.UserRole{}))
	}

	// if get list
	var offset = sdk.ParseInt(req.GetParam("offset"), 0)
	var limit = sdk.ParseInt(req.GetParam("limit"), 10)
	var reverse = req.GetParam("reverse") == "true"
	var getTotal = req.GetParam("getTotal") == "true"
	var result = []model.UserRole{}
	
	// setup query
	var q = model.UserRole{}
	var qStr = req.GetParam("q")
	if qStr != "" {
		err := json.Unmarshal([]byte(qStr), &q)
		if err != nil {
			q = model.UserRole{}
		}
	}
	var source = GetActionSource(req)
	q.OrgCode = source.OrgCode
	q.AppCode = source.Partner.AppCode
	query, err := model.UserRoleDB.Q(q, offset, limit, reverse)
	if err == nil {
		err = query.All(&result)
	}
	response := model.UserRoleDB.R(result, err)

	if getTotal {
		count, err := model.UserRoleDB.Count(q)
		if err == nil {
			response.Total = int64(count)
		}
	}

	return resp.Respond(response)

}

// UserRolePut process PUT method on /iam/v1/user
func UserRolePut(req sdk.APIRequest, resp sdk.APIResponder) error {

	// parse input
	var user model.UserRole
	err := req.GetContent(&user)
	if err != nil {
		resp.Respond(&sdk.APIResponse{
			Status:  sdk.APIStatus.Invalid,
			Message: "Invalid input",
		})
		return err
	}

	if user.Status != "" && user.Status != "ACTIVE" && user.Status != "INACTIVE" {
		user.Status = "ACTIVE"
	}

	// do action
	return resp.Respond(model.UserRoleDB.U(user.ID.Hex(), user))
}

// UserRolePost process POST method on /iam/v1/user
func UserRolePost(req sdk.APIRequest, resp sdk.APIResponder) error {

	// parse input
	var user model.UserRole
	err := req.GetContent(&user)
	if err != nil {
		resp.Respond(&sdk.APIResponse{
			Status:  sdk.APIStatus.Invalid,
			Message: "Invalid input",
		})
		return err
	}

	if user.Status != "ACTIVE" && user.Status != "INACTIVE" {
		user.Status = "ACTIVE"
	}

	// do action
	return resp.Respond(model.UserRoleDB.I(&user))
}