package api

import (
	"strings"

	"github.com/globalsign/mgo/bson"
	"gitlab.ghn.vn/common-projects/go-sdk/sdk"
	"gitlab.ghn.vn/hub-system/iam/client"
	"gitlab.ghn.vn/hub-system/iam/model"
)

type selfInfo struct {
	User        *model.User                  `json:"user"`
	Roles       []*model.Role                `json:"roles"`
	Permissions map[string]*model.Permission `json:"permissions"`
}

// UserGetSelf process GET method on /iam/v1/user/self
func UserGetSelf(req sdk.APIRequest, resp sdk.APIResponder) error {
	var source = GetActionSource(req)
	var info = &selfInfo{
		User: source.User,
	}

	info.Roles = []*model.Role{}
	info.Permissions = map[string]*model.Permission{}

	var userRole = model.UserRole{}
	var roleQuery = model.UserRole{
		AppCode: source.Partner.AppCode,
		OrgCode: source.OrgCode,
		UserID:  source.User.UniqueID,
	}
	q, _ := model.UserRoleDB.Q(roleQuery, 0, 1, false)
	err := q.One(&userRole)
	if err == nil {
		for _, roleCode := range userRole.RoleCodes {
			role := model.GetCachedRole(source.Partner.AppCode, roleCode)
			if role != nil {
				info.Roles = append(info.Roles, role)

				for _, permCode := range role.Permissions {
					permission := model.GetCachedPermission(source.Partner.AppCode, permCode)
					if permission != nil {
						info.Permissions[permCode] = permission
					}
				}
			}
		}
	}

	resp.Respond(&sdk.APIResponse{
		Status:  sdk.APIStatus.Ok,
		Message: "Get self data successfully.",
		Data:    []*selfInfo{info},
	})
	return nil
}

// GHNUserGet process GET method on /iam/v1/user
func GHNUserGet(req sdk.APIRequest, resp sdk.APIResponder) error {
	ghnResp := client.GetGHNUser(req.GetParam("id"))

	if ghnResp.Status == sdk.APIStatus.Ok {
		return resp.Respond(model.UserDB.G(ghnResp.Data.([]string)[0], &model.User{}))
	}

	return resp.Respond(ghnResp)
}

// UserGet process GET method on /iam/v1/user
func UserGet(req sdk.APIRequest, resp sdk.APIResponder) error {

	// check if get by id
	idStr := req.GetParam("id")
	if idStr != "" {
		if strings.Contains(idStr, ",") {
			idsStr := strings.Split(idStr, ",")
			multiResp := &sdk.APIResponse{
				Data: []interface{}{},
			}
			for _, id := range idsStr {
				uResp := model.UserDB.G(id, &model.User{})
				if uResp.Status == sdk.APIStatus.Ok {
					multiResp.Data = append(multiResp.Data.([]interface{}), uResp.Data.([]interface{})[0])
				}
			}
			lenR := len(multiResp.Data.([]interface{}))
			if lenR > 0 {
				multiResp.Status = sdk.APIStatus.Ok
				multiResp.Message = "Get User sucessfully."
				if lenR < len(idsStr) {
					multiResp.ErrorCode = "PARTITIAL_NOT_FOUND"
				}
			} else {
				multiResp.Status = sdk.APIStatus.NotFound
				multiResp.Message = "Users are not found."
			}

			return resp.Respond(multiResp)
		}
		return resp.Respond(model.UserDB.G(idStr, &model.User{}))
	}

	idStr = req.GetParam("uniqueId")
	if idStr != "" {
		if strings.Contains(idStr, ",") {
			idsStr := strings.Split(idStr, ",")
			multiResp := &sdk.APIResponse{
				Data: []model.User{},
			}
			for _, id := range idsStr {
				uResp := model.GetUserByUniqueID(id)
				if uResp.Status == sdk.APIStatus.Ok {
					multiResp.Data = append(multiResp.Data.([]model.User), uResp.Data.([]model.User)[0])
				}
			}
			lenR := len(multiResp.Data.([]model.User))
			if lenR > 0 {
				multiResp.Status = sdk.APIStatus.Ok
				multiResp.Message = "Get User sucessfully."
				if lenR < len(idsStr) {
					multiResp.ErrorCode = "PARTITIAL_NOT_FOUND"
				}
			} else {
				multiResp.Status = sdk.APIStatus.NotFound
				multiResp.Message = "Users are not found."
			}

			return resp.Respond(multiResp)
		}

		return resp.Respond(model.GetUserByUniqueID(idStr))
	}

	// if get list
	var offset = sdk.ParseInt(req.GetParam("offset"), 0)
	var limit = sdk.ParseInt(req.GetParam("limit"), 10)
	var reverse = req.GetParam("reverse") == "true"
	var getTotal = req.GetParam("getTotal") == "true"
	var result = make([]model.User, limit)

	query, err := model.UserDB.Q(bson.M{}, offset, limit, reverse)
	if err == nil {
		err = query.All(&result)
	}
	response := model.UserDB.R(result, err)

	if getTotal {
		count, err := model.UserDB.CountAll()
		if err == nil {
			response.Total = int64(count)
		}
	}

	return resp.Respond(response)

}

// UserPut process PUT method on /iam/v1/user
func UserPut(req sdk.APIRequest, resp sdk.APIResponder) error {

	// parse input
	var user model.User
	err := req.GetContent(&user)
	if err != nil {
		resp.Respond(&sdk.APIResponse{
			Status:  sdk.APIStatus.Invalid,
			Message: "Invalid input",
		})
		return err
	}

	// do action
	return resp.Respond(model.UserDB.U(user.ID.Hex(), user))
}

// UserPost process POST method on /iam/v1/user
func UserPost(req sdk.APIRequest, resp sdk.APIResponder) error {

	// parse input
	var user model.User
	err := req.GetContent(&user)
	if err != nil {
		resp.Respond(&sdk.APIResponse{
			Status:  sdk.APIStatus.Invalid,
			Message: "Invalid input",
		})
		return err
	}

	// do action
	return resp.Respond(model.UserDB.I(&user))
}
