package client

import (
	"fmt"
	"github.com/globalsign/mgo/bson"
	"gitlab.ghn.vn/hub-system/iam/model"
	"encoding/json"
	"time"

	"gitlab.ghn.vn/common-projects/go-sdk/sdk"
	"gitlab.ghn.vn/hub-system/iam/config"
)

var ghnUserClient *sdk.RestClient

func getGhnUserClient() *sdk.RestClient {
	if ghnUserClient == nil {
		ghnUserClient = sdk.NewRESTClient(
			config.Config.OutboundURL["ghn-user-profile"],
			"ghn_user_profile",
			2*time.Second,
			2,
			1*time.Second,
		)
		// ghnUserClient.SetDebug(os.Getenv("env") == "stg")
	}
	fmt.Println(config.Config.OutboundURL["ghn-user-profile"])
	return ghnUserClient
}

type GhnUser struct {
	Fullname string `json:"fullname"`
	Phone    string `json:"phone"`
}

type GhnUserResult struct {
	ErrorMessage string `json:"ErrorMessage"`
	ResultCode int     `json:"code"`
	Data       GhnUser `json:"data"`
}

//GetGHNUser ...
func GetGHNUser(ssoId string) *sdk.APIResponse {
	client := getGhnUserClient()

	// setup body
	params := make(map[string]string)
	params["apikey"] = config.Config.Key["ghnuser-apikey"]
	params["apipass"] = config.Config.Key["ghnuser-apisecret"]
	params["internalid"] = ssoId
	fmt.Println(config.Config.Key["ghnuser-apikey"])
	fmt.Println(config.Config.Key["ghnuser-apisecret"])
	fmt.Println("SSO ID " + ssoId)


	// setup headers
	headers := make(map[string]string)
	headers["Content-Type"] = "application/x-www-form-urlencoded"

	// call api
	result, err := client.MakeHTTPRequest(sdk.HTTPMethods.Post, headers, params, nil, "")
	if err != nil {
		return &sdk.APIResponse{
			Status:  sdk.APIStatus.Error,
			Message: "GHN User Service - Endpoint error: " + err.Error(),
		}
	}

	fmt.Println(result.Body)
	// parse data
	var r = GhnUserResult{}
	err = json.Unmarshal(result.Content, &r)
	if err != nil {
		return &sdk.APIResponse{
			Status:  sdk.APIStatus.Error,
			Message: "GHN User Service - Parse result error: " + err.Error(),
		}
	}

	if r.ErrorMessage != "" {
		return &sdk.APIResponse{
			Status:  sdk.APIStatus.Error,
			Message: "GHN User Service - Return error: " + r.ErrorMessage,
		}
	}

	var user *model.User
	qResult, err := model.UserDB.QueryOne(bson.M{"unique_id": ssoId}, &model.User{})

	// if not exist, add new
	if err != nil || qResult == nil {
		user = &model.User{
			Fullname: r.Data.Fullname,
			PhoneNumber: r.Data.Phone,
			UniqueID: ssoId,
		}
		addResponse := model.UserDB.I(user)
		if addResponse.Status != sdk.APIStatus.Ok {
			return addResponse
		}

		// get again
		qResult, err = model.UserDB.QueryOne(bson.M{"unique_id": ssoId}, &model.User{})

		if err != nil {
			return &sdk.APIResponse{
				Status:  sdk.APIStatus.Error,
				Message: "DB error - Get after insert: " + err.Error(),
			}
		}
	} 

	user = qResult.(*model.User)

	return &sdk.APIResponse{
		Status:  sdk.APIStatus.Ok,
		Data: []string{user.ID.Hex()},
		Message: "Get user successfully.",
	}
}
