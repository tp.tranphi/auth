package client

import (
	"encoding/json"
	"time"

	"gitlab.ghn.vn/common-projects/go-sdk/sdk"
	"gitlab.ghn.vn/hub-system/iam/config"
)

var t62VerifyClient *sdk.RestClient

func getT62VerifyClient() *sdk.RestClient {
	if t62VerifyClient == nil {
		t62VerifyClient = sdk.NewRESTClient(
			config.Config.OutboundURL["check-t62"],
			"sso_verify",
			2*time.Second,
			2,
			1*time.Second,
		)

		// t62VerifyClient.SetDebug(os.Getenv("env") == "stg")
	}
	return t62VerifyClient
}

type t62Result struct {
	ErrorMessage string `json:"ErrorMessage"`
	SsoID        string `json:"Username"`
}

// VerifyT62 ...
func VerifyT62(t62 string) *sdk.APIResponse {
	client := getT62VerifyClient()

	// setup body
	body := make(map[string]string)
	body["AppKey"] = config.Config.Key["sso-appkey"]
	body["Token"] = t62

	// setup headers
	headers := make(map[string]string)
	headers["Content-Type"] = "application/json"

	// call api
	result, err := client.MakeHTTPRequest(sdk.HTTPMethods.Post, headers, nil, body, "")
	if err != nil {
		return &sdk.APIResponse{
			Status:  sdk.APIStatus.Error,
			Message: "Endpoint error: " + err.Error(),
		}
	}

	// parse data
	var r = t62Result{}
	err = json.Unmarshal(result.Content, &r)
	if err != nil || r.ErrorMessage != "" {
		message := r.ErrorMessage
		if err != nil {
			message = err.Error()
		}
		return &sdk.APIResponse{
			Status:  sdk.APIStatus.Error,
			Message: "Parse result error: " + message,
		}
	}

	return &sdk.APIResponse{
		Status:  sdk.APIStatus.Ok,
		Message: "Get SSO successfully.",
		Data:    []string{r.SsoID},
	}
}
