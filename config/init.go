package config

import "os"

type config struct {
	OutboundURL map[string]string
	Key         map[string]string
	Number      map[string]int64
}

// Config main config object
var Config config

func init() {
	env := os.Getenv("env")

	switch env {

	// config for staging
	case "stg":
		Config = config{
			OutboundURL: map[string]string{
				"check-t62":        "https://hr.ghn.vn/Sso/Verify",
				"ghn-user-profile": "https://nguoikhaipha.com/api/employee/detail",
			},
			Key: map[string]string{
				"sso-appkey":        "BB17y1A9A0128b7677C940784CE11A28DE2B3",
				"ghnuser-apikey":    "5fda5f74dd581d3152d80ad33d72dec1",
				"ghnuser-apisecret": "01394436547414030",
			},
			Number: map[string]int64{
				"session-expired-hour": 7 * 24,
			},
		}

		// config for uat
	case "uat":
		Config = config{
			OutboundURL: map[string]string{
				"check-t62":        "https://hr.ghn.vn/Sso/Verify",
				"ghn-user-profile": "https://nguoikhaipha.com/api/employee/detail",
			},
			Key: map[string]string{
				"sso-appkey":        "BB17y1A9A0128b7677C940784CE11A28DE2B3",
				"ghnuser-apikey":    "5fda5f74dd581d3152d80ad33d72dec1",
				"ghnuser-apisecret": "01394436547414030",
			},
			Number: map[string]int64{
				"session-expired-hour": 7 * 24,
			},
		}

		// config for production
	case "prd":
		Config = config{
			OutboundURL: map[string]string{
				"check-t62":        "https://hr.ghn.vn/Sso/Verify",
				"ghn-user-profile": "https://nguoikhaipha.com/api/employee/detail",
			},
			Key: map[string]string{
				"sso-appkey":        "BB17y1A9A0128b7677C940784CE11A28DE2B3",
				"ghnuser-apikey":    "5fda5f74dd581d3152d80ad33d72dec1",
				"ghnuser-apisecret": "01394436547414030",
			},
			Number: map[string]int64{
				"session-expired-hour": 7 * 24,
			},
		}
	}
}
