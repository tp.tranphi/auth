module gitlab.ghn.vn/hub-system/iam

go 1.12

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	gitlab.ghn.vn/common-projects/go-sdk v0.0.82
)
