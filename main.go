package main

import (
	"gitlab.ghn.vn/hub-system/iam/api"
	"encoding/json"
	"encoding/base64"
	"os"
	"strings"
	"time"
	"fmt"
	"gitlab.ghn.vn/common-projects/go-sdk/sdk"
	"gitlab.ghn.vn/hub-system/iam/model"
)

func info(req sdk.APIRequest, res sdk.APIResponder) error {
	return res.Respond(&sdk.APIResponse{
		Status:  sdk.APIStatus.Ok,
		Message: "Identity Access Management (IAM) Service. Code version = " + os.Getenv("version") + ". Started at " + startTime.String(),
	})
}

var env string
var config map[string]string
var startTime time.Time

// onDBConnected function that handle on connected to DB event
func onDBConnected(s *sdk.DBSession) error {

	fmt.Println("DB connected.")

	dbname := "hub_" + env + "_iam"

	// init user model
	model.UserDB.DBName = dbname
	model.InitUserModel(s)

	// init role model
	model.RoleDB.DBName = dbname
	model.InitRoleModel(s)
	
	// init permission model
	model.PermissionDB.DBName = dbname
	model.InitPermissionModel(s)

	// init system partner
	model.SystemPartnerDB.DBName = dbname
	model.InitSystemPartnerModel(s)

	// init org
	model.OrganizationDB.DBName = dbname
	model.InitOrganizationModel(s)

	// init user role
	model.UserRoleDB.DBName = dbname
	model.InitUserRoleModel(s)

	// init system
	model.AppDB.DBName = dbname
	model.InitAppModel(s)

	// warm up
	model.WarmUpCacheValues(false)
	go model.WarmUpCacheValues(true)

	fmt.Println("DB inited")

	return nil
}


func main() {

	startTime = time.Now()

	// get config & env
	env = os.Getenv("env")
	configStr := os.Getenv("config")
	decoded, _ := base64.URLEncoding.DecodeString(configStr)
	json.Unmarshal(decoded, &config)

	// setup new app
	var app = sdk.NewApp("Hub / IAM service")

	// setup DB
	var db = app.SetupDBClient(sdk.DBConfiguration{
		Address:  strings.Split(config["dbHost"], ","),
		Username: config["dbUser"],
		Password: config["dbPassword"],
	})
	db.OnConnected(onDBConnected)

	// setup API Server
	protocol := os.Getenv("protocol")
	if protocol == "" {
		protocol = "THRIFT"
	}

	var server, _ = app.SetupAPIServer(protocol)
	server.SetHandler(sdk.APIMethod.GET, "/iam/v1/api-info", info)

	// for authorize
	server.SetHandler(sdk.APIMethod.POST, "/iam/v1/authorization", api.AuthorizationPost)
	server.SetHandler(sdk.APIMethod.GET, "/iam/v1/system-partner/access-token", api.AccessTokenGet)
	server.SetHandler(sdk.APIMethod.GET, "/iam/v1/system-partner", api.SystemPartnerGet)
	server.SetHandler(sdk.APIMethod.POST, "/iam/v1/system-partner", api.SystemPartnerPost)
	
	// organization
	server.SetHandler(sdk.APIMethod.GET, "/iam/v1/organization", api.OrganizationGet)
	server.SetHandler(sdk.APIMethod.POST, "/iam/v1/organization", api.OrganizationPost)
	server.SetHandler(sdk.APIMethod.PUT, "/iam/v1/organization", api.OrganizationPut)

	// user
	server.SetHandler(sdk.APIMethod.GET, "/iam/v1/user/me", api.UserGetSelf)
	server.SetHandler(sdk.APIMethod.GET, "/iam/v1/user", api.UserGet)
	server.SetHandler(sdk.APIMethod.GET, "/iam/v1/user/ghn", api.GHNUserGet)
	server.SetHandler(sdk.APIMethod.POST, "/iam/v1/user", api.UserPost)
	server.SetHandler(sdk.APIMethod.PUT, "/iam/v1/user", api.UserPut)

	// role
	server.SetHandler(sdk.APIMethod.GET, "/iam/v1/role", api.RoleGet)
	server.SetHandler(sdk.APIMethod.POST, "/iam/v1/role", api.RolePost)
	server.SetHandler(sdk.APIMethod.PUT, "/iam/v1/role", api.RolePut)
	server.SetHandler(sdk.APIMethod.DELETE, "/iam/v1/role", api.RoleDelete)

	// permssion
	server.SetHandler(sdk.APIMethod.GET, "/iam/v1/permission", api.PermissionGet)
	server.SetHandler(sdk.APIMethod.POST, "/iam/v1/permission", api.PermissionPost)
	server.SetHandler(sdk.APIMethod.PUT, "/iam/v1/permission", api.PermissionPut)
	server.SetHandler(sdk.APIMethod.DELETE, "/iam/v1/permission", api.PermissionDelete)

	// app
	server.SetHandler(sdk.APIMethod.GET, "/iam/v1/app", api.AppGet)
	server.SetHandler(sdk.APIMethod.POST, "/iam/v1/app", api.AppPost)
	server.SetHandler(sdk.APIMethod.PUT, "/iam/v1/app", api.AppPut)
	server.SetHandler(sdk.APIMethod.DELETE, "/iam/v1/app", api.AppDelete)

	// session
	server.SetHandler(sdk.APIMethod.GET, "/iam/v1/session/extended", api.SessionExtendGet)
	server.SetHandler(sdk.APIMethod.POST, "/iam/v1/session/t62", api.SessionT62Post)

	// user-role
	server.SetHandler(sdk.APIMethod.GET, "/iam/v1/user-role", api.UserRoleGet)
	server.SetHandler(sdk.APIMethod.POST, "/iam/v1/user-role", api.UserRolePost)
	server.SetHandler(sdk.APIMethod.PUT, "/iam/v1/user-role", api.UserRolePut)

	server.PreRequest(api.PreRequest)

	// expose
	server.Expose(80)

	// launch app
	app.Launch()
}
