package model

import (
	"time"

	"github.com/globalsign/mgo"

	"gitlab.ghn.vn/common-projects/go-sdk/sdk"
	"github.com/globalsign/mgo/bson"
)

//App represent a user of the system
type App struct {
	ID              bson.ObjectId `json:"id,omitempty" bson:"_id,omitempty"`
	LastUpdatedTime *time.Time     `json:"lastUpdatedTime,omitempty" bson:"last_updated_time,omitempty"`
	CreatedTime     *time.Time     `json:"createdTime,omitempty" bson:"created_time,omitempty"`

	Name        string `json:"name,omitempty" bson:"name,omitempty"`
	Description string `json:"description,omitempty" bson:"description,omitempty"`
	Code        string `json:"code,omitempty" bson:"code,omitempty"` // unique & readable ID of partner. Ex: GHN_EXPRESS, AHAMOVE, ...
	Icon        string `json:"icon,omitempty" bson:"icon,omitempty"` // unique & readable ID of partner. Ex: GHN_EXPRESS, AHAMOVE, ...
}

//AppDB represent DB repo of AppPartner model
var AppDB = &sdk.DBModel{
	ColName: "app",
}

//AppCache Cached App list
var AppCache map[string]*App

//InitAppModel ...
func InitAppModel(s *sdk.DBSession) {

	// init db
	AppDB.Init(s)

	// get collection & create index
	session := AppDB.GetFreshSession()
	defer session.Close()
	col, err := AppDB.GetColWith(session)
	if err == nil {
		col.EnsureIndex(mgo.Index{
			Key:        []string{"code"},
			Background: true,
			Unique:     true,
		})
	}
}
