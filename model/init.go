package model

import (
	"time"

	"github.com/globalsign/mgo/bson"
)

func init() {

}

//WarmUpCacheValues this func update permissions, roles & api system to cache to speed up
func WarmUpCacheValues(loop bool) {
	for {
		
		// do once per 1 minutes
		if loop {
			time.Sleep(1 * time.Minute)
		}
		
		// cache permission
		var permissionList []Permission
		var tmpPermissionCache = make(map[string]*Permission)
		PermissionDB.Query(bson.M{}, 0, 0, false, &permissionList)
		for i := 0; i < len(permissionList); i++ {
			tmpPermissionCache[permissionList[i].AppCode+"."+permissionList[i].Code] = &permissionList[i]
		}
		PermissionCache = tmpPermissionCache

		// cache role
		var roleList []Role
		var tmpRoleCache = make(map[string]*Role)
		RoleDB.Query(bson.M{}, 0, 0, false, &roleList)
		for i := 0; i < len(roleList); i++ {
			tmpRoleCache[roleList[i].AppCode+"."+roleList[i].Code] = &roleList[i]
		}
		RoleCache = tmpRoleCache

		// cache system partner
		var partnerList []SystemPartner
		var tmpPartnerCache = make(map[string]*SystemPartner)
		SystemPartnerDB.Query(bson.M{}, 0, 0, false, &partnerList)
		for i := 0; i < len(partnerList); i++ {
			tmpPartnerCache[partnerList[i].Code] = &partnerList[i]
		}
		SystemPartnerCache = tmpPartnerCache

		// cache app partner
		var appList []App
		var tmpAppCache = make(map[string]*App)
		AppDB.Query(bson.M{}, 0, 0, false, &appList)
		for i := 0; i < len(appList); i++ {
			tmpAppCache[appList[i].Code] = &appList[i]
		}
		AppCache = tmpAppCache

		// cache organization
		var organizationList []Organization
		var tmpOrganizationCache = make(map[string]*Organization)
		OrganizationDB.Query(bson.M{}, 0, 0, false, &organizationList)
		for i := 0; i < len(organizationList); i++ {
			tmpOrganizationCache[roleList[i].Code] = &organizationList[i]
		}
		OrganizationCache = tmpOrganizationCache

		

		if !loop {
			return
		}

	}
}

// ActionSource source info transferred between Gateway & inside Services
type ActionSource struct {
	Partner *SystemPartner `json:"partner"`
	User    *User          `json:"user"`
	OrgCode string         `json:"orgCode"`
}
