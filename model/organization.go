package model

import (
	"time"

	"gitlab.ghn.vn/common-projects/go-sdk/sdk"
	mgo "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

// Organization represent a user of the system
type Organization struct {
	ID              bson.ObjectId `json:"id,omitempty" bson:"_id,omitempty"`
	LastUpdatedTime *time.Time     `json:"lastUpdatedTime,omitempty" bson:"last_updated_time,omitempty"`
	CreatedTime     *time.Time     `json:"createdTime,omitempty" bson:"created_time,omitempty"`

	Name        string `json:"name,omitempty" bson:"name,omitempty"`
	Code        string `json:"code,omitempty" bson:"code,omitempty"`
	Description string `json:"description,omitempty" bson:"description,omitempty"`
}

// OrganizationDB represent DB repo of this model
var OrganizationDB = &sdk.DBModel{
	ColName: "organization",
}

// OrganizationCache Cached organization list
var OrganizationCache map[string]*Organization

// InitOrganizationModel ...
func InitOrganizationModel(s *sdk.DBSession) {
	OrganizationDB.Init(s)
	session := OrganizationDB.GetFreshSession()
	defer session.Close()
	col, err := OrganizationDB.GetColWith(session)
	if err == nil {
		col.EnsureIndex(mgo.Index{
			Key:        []string{"code"},
			Background: true,
			Unique:     true,
		})
	}
}
