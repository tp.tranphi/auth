package model

import (
	"time"

	"gitlab.ghn.vn/common-projects/go-sdk/sdk"
	mgo "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

// Permission represent a access control of the system
type Permission struct {
	ID              bson.ObjectId `json:"id,omitempty" bson:"_id,omitempty"`
	LastUpdatedTime *time.Time    `json:"lastUpdatedTime,omitempty" bson:"last_updated_time,omitempty"`
	CreatedTime     *time.Time    `json:"createdTime,omitempty" bson:"created_time,omitempty"`

	Name         string `json:"name,omitempty" bson:"name,omitempty"`
	Code         string `json:"code,omitempty" bson:"code,omitempty"`
	Description  string `json:"description,omitempty" bson:"description,omitempty"`
	Path         string `json:"path,omitempty" bson:"path,omitempty"`
	Method       string `json:"method,omitempty" bson:"method,omitempty"`
	CanSkipLogin bool   `json:"canSkipLogin,omitempty" bson:"can_skip_login,omitempty"`
	AppCode      string `json:"appCode,omitempty" bson:"app_code,omitempty"`
}

// PermissionDB represent DB repo of Permission model
var PermissionDB = &sdk.DBModel{
	ColName: "permission",
}

// PermissionCache Cached permission list
var PermissionCache map[string]*Permission

//GetCachedPermission get permission from cache
func GetCachedPermission(systemCode string, permissionCode string) *Permission {
	return PermissionCache[systemCode+"."+permissionCode]
}

// InitPermissionModel setup db
func InitPermissionModel(s *sdk.DBSession) {
	PermissionDB.Init(s)
	session := PermissionDB.GetFreshSession()
	defer session.Close()
	col, err := PermissionDB.GetColWith(session)
	if err == nil {
		col.EnsureIndex(mgo.Index{
			Key:        []string{"app_code", "code"},
			Background: true,
			Unique:     true,
		})
	}
}
