package model

import (
	"time"

	"gitlab.ghn.vn/common-projects/go-sdk/sdk"
	mgo "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

// Role represent a user of the system
type Role struct {
	ID              bson.ObjectId `json:"id,omitempty" bson:"_id,omitempty"`
	LastUpdatedTime *time.Time    `json:"lastUpdatedTime,omitempty" bson:"last_updated_time,omitempty"`
	CreatedTime     *time.Time    `json:"createdTime,omitempty" bson:"created_time,omitempty"`

	Name        string   `json:"name,omitempty" bson:"name,omitempty"`
	Code        string   `json:"code,omitempty" bson:"code,omitempty"`
	Description string   `json:"description,omitempty" bson:"description,omitempty"`
	Permissions []string `json:"permissions,omitempty" bson:"permissions,omitempty"`
	AppCode     string   `json:"appCode,omitempty" bson:"app_code,omitempty"`
	Type 		string 	 `json:"type,omitempty" bson:"type,omitempty"`
}

// RoleDB represent DB repo of this model
var RoleDB = &sdk.DBModel{
	ColName: "role",
}

// RoleCache Cached role list
var RoleCache map[string]*Role

//GetCachedRole get role from cache
func GetCachedRole(systemCode string, roleCode string) *Role {
	return RoleCache[systemCode+"."+roleCode]
}

// InitRoleModel ...
func InitRoleModel(s *sdk.DBSession) {
	RoleDB.Init(s)
	session := RoleDB.GetFreshSession()
	defer session.Close()
	col, err := RoleDB.GetColWith(session)
	if err == nil {
		col.EnsureIndex(mgo.Index{
			Key:        []string{"app_code", "code"},
			Background: true,
			Unique:     true,
		})
	}
}
