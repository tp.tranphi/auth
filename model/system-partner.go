package model

import (
	"time"

	"github.com/globalsign/mgo"

	"gitlab.ghn.vn/common-projects/go-sdk/sdk"
	"github.com/globalsign/mgo/bson"
)

// SystemPartner represent a user of the system
type SystemPartner struct {
	ID              bson.ObjectId `json:"id,omitempty" bson:"_id,omitempty"`
	LastUpdatedTime *time.Time     `json:"lastUpdatedTime,omitempty" bson:"last_updated_time,omitempty"`
	CreatedTime     *time.Time     `json:"createdTime,omitempty" bson:"created_time,omitempty"`

	Name     string `json:"name,omitempty" bson:"name,omitempty"`
	Code     string `json:"code,omitempty" bson:"code,omitempty"`     // unique & readable ID of partner. Ex: GHN_EXPRESS, AHAMOVE, ...
	Public   string `json:"public,omitempty" bson:"public,omitempty"` // used to call generate-access-token API
	Secret   string `json:"secret,omitempty" bson:"secret,omitempty"` // used to generate basic access token
	RoleCode string `json:"roleCode,omitempty" bson:"role_code,omitempty"`
	AppCode  string   `json:"appCode,omitempty" bson:"app_code,omitempty"`
}

// SystemPartnerDB represent DB repo of SystemPartner model
var SystemPartnerDB = &sdk.DBModel{
	ColName: "system_partner",
}

// SystemPartnerCache Cached SystemPartner list
var SystemPartnerCache map[string]*SystemPartner

//InitSystemPartnerModel setup indexes
func InitSystemPartnerModel(s *sdk.DBSession) {

	// init db
	SystemPartnerDB.Init(s)

	// get collection & create index
	session := SystemPartnerDB.GetFreshSession()
	defer session.Close()
	col, err := SystemPartnerDB.GetColWith(session)
	if err == nil {
		col.EnsureIndex(mgo.Index{
			Key:        []string{"code"},
			Background: true,
			Unique:     true,
		})
	}
}
