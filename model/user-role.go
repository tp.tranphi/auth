package model

import (
	"time"

	"gitlab.ghn.vn/common-projects/go-sdk/sdk"
	mgo "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

//UserRole represent a user of the system
type UserRole struct {
	ID              bson.ObjectId `json:"id,omitempty" bson:"_id,omitempty"`
	LastUpdatedTime *time.Time    `json:"lastUpdatedTime,omitempty" bson:"last_updated_time,omitempty"`
	CreatedTime     *time.Time    `json:"createdTime,omitempty" bson:"created_time,omitempty"`

	UserID    string   `json:"userId,omitempty" bson:"user_id,omitempty"`
	RoleCodes []string `json:"roleCodes,omitempty" bson:"role_codes,omitempty"`
	Status  string     `json:"status,omitempty" bson:"status,omitempty"`
	OrgCode   string   `json:"orgCode,omitempty" bson:"org_code,omitempty"`
	AppCode   string   `json:"appCode,omitempty" bson:"app_code,omitempty"`
	LastActive time.Time `json:"lastActive,omitempty" bson:"last_active,omitempty"`
}

//UserRoleDB represent DB repo of this model
var UserRoleDB = &sdk.DBModel{
	ColName: "user_role",
}

// UserRoleCache Cached UserRole list
// key: app code / org code / unique id of user
// value: active role
var UserRoleCache = sdk.NewLCache(10000, 120)

//InitUserRoleModel ...
func InitUserRoleModel(s *sdk.DBSession) {
	UserRoleDB.Init(s)
	session := UserRoleDB.GetFreshSession()
	defer session.Close()
	col, err := UserRoleDB.GetColWith(session)
	if err == nil {
		col.EnsureIndex(mgo.Index{
			Key:        []string{"app_code", "org_code", "user_id"},
			Background: true,
			Unique:     true,
		})
		col.EnsureIndex(mgo.Index{
			Key:        []string{"app_code", "org_code"},
			Background: true,
		})
	}
}
