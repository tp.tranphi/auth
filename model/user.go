package model

import (
	"time"

	mgo "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"gitlab.ghn.vn/common-projects/go-sdk/sdk"
)

// User represent a user of the system
type User struct {
	ID              bson.ObjectId `json:"id,omitempty" bson:"_id,omitempty"`
	LastUpdatedTime *time.Time    `json:"lastUpdatedTime,omitempty" bson:"last_updated_time,omitempty"`
	CreatedTime     *time.Time    `json:"createdTime,omitempty" bson:"created_time,omitempty"`

	Fullname      string            `json:"fullname,omitempty" bson:"fullname,omitempty"`
	PhoneNumber   string            `json:"phoneNumber,omitempty" bson:"phone_number,omitempty"`
	IsSystemAdmin bool              `json:"isSystemAdmin,omitempty" bson:"is_system_admin,omitempty"`
	UniqueID      string            `json:"uniqueId,omitempty" bson:"unique_id,omitempty"`
	LinkedInfo    map[string]string `json:"linkedInfo,omitempty" bson:"linked_info,omitempty"`
	LastLoginTime *time.Time        `json:"lastLoginTime,omitempty" bson:"last_login_time,omitempty"`
}

// UserDB represent DB repo of this model
var UserDB = &sdk.DBModel{
	ColName: "user",
}

// UserCache Cached UserRole list
// key: mongoid of user
// value: user
var UserCache = sdk.NewLCache(10000, 300)

// InitUserModel ...
func InitUserModel(s *sdk.DBSession) {
	UserDB.Init(s)
	session := UserDB.GetFreshSession()
	defer session.Close()
	col, err := UserDB.GetColWith(session)
	if err == nil {
		col.EnsureIndex(mgo.Index{
			Key:        []string{"unique_id"},
			Background: true,
			Unique:     true,
		})
	}
}

// GetUserByUniqueID ...
func GetUserByUniqueID(uniqueID string) *sdk.APIResponse {
	query, err := UserDB.Q(User{
		UniqueID: uniqueID,
	}, 0, 1, false)
	if err != nil {
		return UserDB.R(nil, err)
	}

	var one = User{}
	err = query.One(&one)
	return UserDB.R([]User{one}, err)
}
