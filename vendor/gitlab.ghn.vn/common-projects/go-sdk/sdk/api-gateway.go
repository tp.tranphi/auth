package sdk

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

	mgo "github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

// GWRoute ...
type GWRoute struct {
	Path     string `json:"path" bson:"path"`
	Address  string `json:"address" bson:"address"`
	Protocol string `json:"protocol" bson:"protocol"`
}

// APIGateway ...
type APIGateway struct {
	server       APIServer
	conf         *mgo.Collection
	routes       []GWRoute
	clients      map[string]APIClient
	preForward   Handler
	onBadGateway Handler
	debug        bool
}

// NewAPIGateway Create new API Gateway
func NewAPIGateway(server APIServer) *APIGateway {
	var gw = &APIGateway{
		server:  server,
		clients: make(map[string]APIClient),
		debug:   false,
	}
	server.PreRequest(gw.route)
	return gw
}

// SetDebug  ...
func (gw *APIGateway) SetDebug(val bool) {
	gw.debug = val
}

// GetGWRoutes get current routes
func (gw *APIGateway) GetGWRoutes() []GWRoute {
	return gw.routes
}

// LoadConfigFromDB ..
func (gw *APIGateway) LoadConfigFromDB(col *mgo.Collection) {
	gw.conf = col
	go gw.scanConfig()
}

// LoadConfigFromObject ..
func (gw *APIGateway) LoadConfigFromObject(routes []GWRoute) {
	gw.routes = routes
}

// SetPreForward set pre-handler for filter / authen / author
func (gw *APIGateway) SetPreForward(hdl Handler) {
	gw.preForward = hdl
}

// SetBadGateway set handler for bad gateway cases
func (gw *APIGateway) SetBadGateway(hdl Handler) {
	gw.onBadGateway = hdl
}

func (gw *APIGateway) scanConfig() {
	for true {
		time.Sleep(10 * time.Second)
		var result []GWRoute
		gw.conf.Find(bson.M{}).All(&result)
		if result != nil {
			gw.routes = result
		}
		if gw.debug {
			bytes, _ := json.Marshal(result)
			fmt.Println("Got routes: " + string(bytes))
		}
	}
}

func (gw *APIGateway) getClient(routeInfo GWRoute) APIClient {
	if gw.clients[routeInfo.Path] != nil {
		return gw.clients[routeInfo.Path]
	}
	config := &APIClientConfiguration{
		Address:       routeInfo.Address,
		Protocol:      routeInfo.Protocol,
		Timeout:       5000,
		MaxRetry:      2,
		WaitToRetry:   1500,
		MaxConnection: 100,
	}

	client := NewAPIClient(config)
	client.SetDebug(gw.debug)

	gw.clients[routeInfo.Path] = client
	return client
}

func (gw *APIGateway) route(req APIRequest, res APIResponder) error {

	path := req.GetPath()
	method := req.GetMethod()

	if method.Value == APIMethod.OPTIONS.Value {
		return gw.onBadGateway(req, res)
	}

	if gw.debug {
		fmt.Println("Receive Method / Path = " + method.Value + " => " + path)
		bytes, _ := json.Marshal(gw.routes)
		fmt.Println("Current routes: " + string(bytes))
	}
	for i := 0; i < len(gw.routes); i++ {
		if strings.HasPrefix(path, gw.routes[i].Path) {

			if gw.debug {
				fmt.Println(" => Found route: " + gw.routes[i].Protocol + " / " + gw.routes[i].Address)
			}

			// filter, for authen / author ....
			if gw.preForward != nil {
				if gw.debug {
					fmt.Println(" + Gateway has pre-handler")
				}
				err := gw.preForward(req, res)
				if err != nil {
					if gw.debug {
						fmt.Println(" => Pre-handler return error: " + err.Error())
					}
					return err
				}
			}

			// forward the request
			client := gw.getClient(gw.routes[i])
			if gw.debug {
				fmt.Println(" + Get client successfully.")
			}

			headers := req.GetAttribute("AddedHeaders")
			if headers != nil {
				headerMap := headers.(map[string]string)
				curHeaders := req.GetHeaders()
				for key, value := range headerMap {
					curHeaders[key] = value
				}
				req = NewOutboundAPIRequest(
					req.GetMethod().Value,
					req.GetPath(),
					req.GetParams(),
					req.GetContentText(),
					curHeaders,
				)
			}

			resp := client.MakeRequest(req)
			if gw.debug {
				fmt.Println(" => Call ended.")
				fmt.Println(" => Result: " + resp.Status + " / " + resp.Message)
			}
			if resp.Headers == nil {
				resp.Headers = make(map[string]string)
			}
			if resp.Headers["X-Execution-Time"] != "" {
				resp.Headers["X-Endpoint-Time"] = resp.Headers["X-Execution-Time"]
			}
			resp.Headers["Access-Control-Allow-Origin"] = "*"
			resp.Headers["Access-Control-Allow-Methods"] = "OPTIONS, GET, POST, PUT, DELETE"
			res.Respond(resp)
			return &Error{Type: "PROXY_FOUND", Message: "Proxy found."}
		}
	}

	// if not found on gateway config
	if gw.onBadGateway != nil {
		return gw.onBadGateway(req, res)
	}

	return nil
}
