package sdk

import (
	"fmt"
	"strconv"
	"sync"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

type APIServer interface {
	PreRequest(Handler) error
	SetHandler(*MethodValue, string, Handler) error
	Expose(int)
	Start(*sync.WaitGroup)
}

// HTTPAPIServer ...
type HTTPAPIServer struct {
	T      string
	Echo   *echo.Echo
	Thrift *ThriftServer
	Port   int
	ID     int
}

func newHTTPAPIServer(id int) APIServer {
	var server = HTTPAPIServer{
		T:    "HTTP",
		Echo: echo.New(),
		ID:   id,
	}
	server.Echo.Use(middleware.Gzip())
	return &server
}

//SetHandle Add api handler
func (server *HTTPAPIServer) SetHandler(method *MethodValue, path string, fn Handler) error {
	var wrapper = &HandlerWrapper{
		handler: fn,
	}

	switch method.Value {
	case APIMethod.GET.Value:
		server.Echo.GET(path, wrapper.processCore)
	case APIMethod.POST.Value:
		server.Echo.POST(path, wrapper.processCore)
	case APIMethod.PUT.Value:
		server.Echo.PUT(path, wrapper.processCore)
	case APIMethod.DELETE.Value:
		server.Echo.DELETE(path, wrapper.processCore)
	}

	return nil
}

//PreRequest ...
func (server *HTTPAPIServer) PreRequest(fn PreHandler) error {

	var preWrapper = &PreHandlerWrapper{
		preHandler: fn,
	}

	server.Echo.Use(func(next echo.HandlerFunc) echo.HandlerFunc {
		preWrapper.next = next
		return preWrapper.processCore
	})
	return nil
}

//Expose Add api handler
func (server *HTTPAPIServer) Expose(port int) {
	server.Port = port
}

//Start Start API server
func (server *HTTPAPIServer) Start(wg *sync.WaitGroup) {
	var ps = strconv.Itoa(server.Port)
	fmt.Println("  [ API Server " + strconv.Itoa(server.ID) + " ] Try to listen at " + ps)
	server.Echo.HideBanner = true
	server.Echo.Start(":" + ps)
	wg.Done()
}

// HandlerWrapper handler object
type HandlerWrapper struct {
	handler Handler
}

// Handler ...
type Handler = func(req APIRequest, res APIResponder) error

// processCore Process basic logic of Echo
func (hw *HandlerWrapper) processCore(c echo.Context) error {
	hw.handler(newHTTPAPIRequest(c), newHTTPAPIResponder(c))
	return nil
}

// PreHandlerWrapper
type PreHandlerWrapper struct {
	preHandler Handler
	next       echo.HandlerFunc
}

// PreHandler ...
type PreHandler = func(req APIRequest, res APIResponder) error

// processCore Process basic logic of Echo
func (hw *PreHandlerWrapper) processCore(c echo.Context) error {
	req := newHTTPAPIRequest(c)
	resp := newHTTPAPIResponder(c)
	err := hw.preHandler(req, resp)
	if err == nil {
		hw.next(c)
	}
	return nil
}
