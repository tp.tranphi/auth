package sdk

import (
	"bytes"
	"compress/gzip"
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/labstack/gommon/log"
)

// RestClient :
type RestClient struct {
	BaseURL   *url.URL
	UserAgent string

	// private
	httpClient   *http.Client
	logger       *log.Logger
	maxRetryTime int
	waitTime     time.Duration // milisecond
	timeOut      time.Duration // milisecond

	debug bool
}

// RequestLogEntry ...
type RequestLogEntry struct {
	ReqURL      string
	ReqMethod   string
	ReqHeader   map[string]string
	ReqFormData map[string]string
	ReqBody     interface{}

	TotalTime  int64
	RetryCount int
	Results    []*CallResult
	ErrorLog   string
}

// CallResult ...
type CallResult struct {
	RespCode     int
	RespBody     string
	ResponseTime int64
	ErrorLog     string
}

//RestResult :
type RestResult struct {
	Body    string
	Content []byte
	Code    int
}

// HTTPMethod ...
type HTTPMethod string

// HTTPMethodEnum ...
type HTTPMethodEnum struct {
	Get    HTTPMethod
	Post   HTTPMethod
	Put    HTTPMethod
	Head   HTTPMethod
	Delete HTTPMethod
	Option HTTPMethod
}

// HTTPMethods Supported HTTP Method
var HTTPMethods = &HTTPMethodEnum{
	Get:    "GET",
	Post:   "POST",
	Put:    "PUT",
	Head:   "HEAD",
	Delete: "DELETE",
	Option: "OPTION",
}

// NewHTTPClient
func NewHTTPClient(config *APIClientConfiguration) APIClient {
	return NewRESTClient(config.Address, config.LoggingCol,
		time.Duration(config.Timeout)*time.Millisecond,
		config.MaxRetry,
		time.Duration(config.WaitToRetry)*time.Millisecond)
}

// NewRESTClient : New instance of restClient
func NewRESTClient(baseURL string, logName string, timeout time.Duration, maxRetryTime int, waitTime time.Duration) *RestClient {

	var restCl RestClient

	if !strings.HasPrefix(baseURL, "http") {
		baseURL = "http://" + baseURL
	}

	u, err := url.Parse(baseURL)
	if err == nil {
		restCl.BaseURL = u
	}
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	restCl.httpClient = &http.Client{
		Transport: tr,
	}

	restCl.SetMaxRetryTime(maxRetryTime)
	restCl.SetWaitTime(waitTime)
	restCl.SetTimeout(timeout)
	restCl.debug = false
	if logName != "" {
		restCl.SetLoggerName(logName)
	}

	return &restCl
}

// addParam
func addParams(baseURL string, params map[string]string) string {
	baseURL += "?"
	p := url.Values{}
	for key, value := range params {
		p.Add(key, value)
	}
	return baseURL + p.Encode()
}

func (entry *RequestLogEntry) addResult(rs *CallResult) {
	entry.Results = append(entry.Results, rs)
}

// ToAPIResponse :
func (c *RestResult) ToAPIResponse() (*APIResponse, error) {
	if c.Code < 200 || c.Code >= 300 {
		return &APIResponse{Status: APIStatus.Error, Message: c.Body}, nil
	}
	var rs *APIResponse
	err := json.Unmarshal(c.Content, &rs)
	if err != nil {
		return nil, err
	}
	return &APIResponse{Status: APIStatus.Ok, Message: rs.Message, Data: rs.Data}, nil
}

// SetLoggerName :
func (c *RestClient) SetLoggerName(loggerName string) {
	l := log.New(loggerName)
	err := os.MkdirAll("./log", os.ModePerm)
	if err == nil {
		file, errLog := os.OpenFile(fmt.Sprintf("./log/%s.log", loggerName), os.O_CREATE|os.O_RDWR|os.O_APPEND, 0666)
		if errLog != nil {
			fmt.Println(errLog.Error())
		}
		l.SetOutput(file)
		l.SetLevel(log.INFO)
	}
	c.logger = l
}

func (c *RestClient) SetDebug(val bool) {
	c.debug = val
}

// SetTimeout :
func (c *RestClient) SetTimeout(timeout time.Duration) {
	c.timeOut = timeout
	c.httpClient.Timeout = timeout * time.Millisecond
}

// SetWaitTime :
func (c *RestClient) SetWaitTime(waitTime time.Duration) {
	c.waitTime = waitTime
}

// SetMaxRetryTime :
func (c *RestClient) SetMaxRetryTime(maxRetryTime int) {
	c.maxRetryTime = maxRetryTime
}

func (c *RestClient) initRequest(method HTTPMethod, headers map[string]string, params map[string]string, body interface{}, path string) (*http.Request, error) {

	u := c.BaseURL.String()
	if path != "" {
		if strings.HasSuffix(u, "/") || strings.HasPrefix(path, "/") {
			u = u + path
		} else {
			u = u + "/" + path
		}
	}
	urlStr := addParams(u, params)

	var buf io.ReadWriter
	if body != nil {
		buf = new(bytes.Buffer)
		err := json.NewEncoder(buf).Encode(body)
		if err != nil {
			return nil, err
		}
	}

	var err error
	var req *http.Request
	if method == HTTPMethods.Post && headers != nil && headers["Content-Type"] == "application/x-www-form-urlencoded" && params != nil && len(params) > 0 {
		data := url.Values{}
		for key, val := range params {
			data.Set(key, val)
		}
		req, err = http.NewRequest(string(method), urlStr, strings.NewReader(data.Encode()))
	} else {
		req, err = http.NewRequest(string(method), urlStr, buf)
	}

	if err != nil {
		return nil, err
	}
	if body != nil {
		req.Header.Set("Content-Type", "application/json")
	}
	req.Header.Set("Accept", "application/json")
	req.Header.Set("User-Agent", "Go-RESTClient/1.0")

	// set header
	for key, value := range headers {
		req.Header.Set(key, value)
	}

	return req, nil
}

// MakeHTTPRequest :
func (c *RestClient) MakeHTTPRequest(method HTTPMethod, headers map[string]string, params map[string]string, body interface{}, path string) (*RestResult, error) {
	// init log
	logEntry := &RequestLogEntry{
		ReqURL:      c.BaseURL.String() + path,
		ReqMethod:   string(method),
		ReqFormData: params,
		ReqHeader:   headers,
		ReqBody:     body,
	}

	if c.logger != nil {
		defer func() {
			parsed, errParse := json.Marshal(logEntry)
			if errParse == nil {
				c.logger.Info(string(parsed))
			}
		}()
	}

	req, reqErr := c.initRequest(method, headers, params, body, path)

	if c.debug {
		fmt.Println(" +++ Init request successfully.")
	}

	if reqErr != nil {
		logEntry.ErrorLog = reqErr.Error()
		return nil, reqErr
	}
	canRetryCount := c.maxRetryTime

	var resp = &http.Response{}
	var err error
	var restResult = RestResult{}

	tstart := time.Now().UnixNano() / 1e6

	for canRetryCount >= 0 {
		// start time
		startCallTime := time.Now().UnixNano() / 1e6
		if c.debug {
			fmt.Println("+++ Let call: " + logEntry.ReqMethod + " " + logEntry.ReqURL)
		}

		// add call result
		callRs := CallResult{}

		// do request
		resp, err = c.httpClient.Do(req)
		if c.debug {
			fmt.Println("+++ HTTP call ended!")
		}
		if err == nil {
			defer resp.Body.Close()
			v, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				fmt.Println("Error parse")
				callRs.ErrorLog = err.Error()
				return nil, err
			}

			if c.debug {
				fmt.Println("+++ IO read ended!")
			}
			restResult := RestResult{
				Code:    resp.StatusCode,
				Body:    string(v),
				Content: v,
			}

			encoding := resp.Header.Get("Content-Encoding")
			if encoding == "gzip" {
				if c.debug {
					fmt.Println("+++ Start to gunzip")
				}
				gr, _ := gzip.NewReader(bytes.NewBuffer(restResult.Content))
				defer gr.Close()
				data, err := ioutil.ReadAll(gr)
				if err != nil {
					return nil, err
				}
				if c.debug {
					fmt.Println("+++ gunzip successfully")
				}
				restResult.Content = data
				restResult.Body = string(data)
			}

			// set call result
			callRs.RespCode = restResult.Code
			callRs.RespBody = restResult.Body

			if c.debug {
				fmt.Println("+++ Read data end, http code: " + string(resp.StatusCode))
			}
			if (resp.StatusCode >= 200 && resp.StatusCode < 300) || (resp.StatusCode >= 400 && resp.StatusCode < 500) {
				// add log
				tend := time.Now().UnixNano() / 1e6
				callRs.ResponseTime = tend - startCallTime
				logEntry.TotalTime = tend - tstart
				if canRetryCount >= 0 {
					logEntry.RetryCount = c.maxRetryTime - canRetryCount
				}
				//sample
				logEntry.addResult(&callRs)
				//return
				return &restResult, err
			}
		} else {
			fmt.Println("HTTP Error: " + err.Error())
			callRs.ErrorLog = err.Error()
		}
		tend := time.Now().UnixNano() / 1e6
		callRs.ResponseTime = tend - startCallTime

		time.Sleep(c.waitTime * time.Millisecond)
		canRetryCount--
		if canRetryCount >= 0 {
			logEntry.RetryCount = c.maxRetryTime - canRetryCount
		}
		logEntry.addResult(&callRs)

	}
	tend := time.Now().UnixNano() / 1e6
	logEntry.TotalTime = tend - tstart
	return &restResult, errors.New("fail to call endpoint API " + logEntry.ReqURL)
}

// MakeRequest
func (c *RestClient) MakeRequest(req APIRequest) *APIResponse {
	var data interface{}
	var reqMethod = req.GetMethod()
	var method HTTPMethod

	switch reqMethod.Value {
	case "GET":
		method = HTTPMethods.Get
	case "PUT":
		method = HTTPMethods.Put
		req.GetContent(&data)
	case "POST":
		method = HTTPMethods.Post
		req.GetContent(&data)
	case "DELETE":
		method = HTTPMethods.Delete
	case "OPTIONS":
		method = HTTPMethods.Option
	}

	if c.debug {
		fmt.Println("Req info: " + reqMethod.Value + " / " + req.GetPath())
		if data != nil {
			fmt.Println("Data not null")
		}
	}

	result, err := c.MakeHTTPRequest(method, req.GetHeaders(), req.GetParams(), data, req.GetPath())

	if err != nil {
		return &APIResponse{
			Status:  APIStatus.Error,
			Message: "HTTP Endpoint Error: " + err.Error(),
		}
	}

	var resp = &APIResponse{}
	err = json.Unmarshal(result.Content, &resp)

	if resp.Status == "" {
		if result.Code >= 500 {
			resp.Status = APIStatus.Error
		} else if result.Code >= 400 {
			if result.Code == 404 {
				resp.Status = APIStatus.NotFound
			} else if result.Code == 403 {
				resp.Status = APIStatus.Forbidden
			} else if result.Code == 401 {
				resp.Status = APIStatus.Unauthorized
			} else {
				resp.Status = APIStatus.Invalid
			}
		} else {
			resp.Status = APIStatus.Ok
		}
	}

	if err != nil {
		return &APIResponse{
			Status:  APIStatus.Error,
			Message: "Response Data Error: " + err.Error(),
			Data:    []string{result.Body},
		}
	}
	return resp
}
