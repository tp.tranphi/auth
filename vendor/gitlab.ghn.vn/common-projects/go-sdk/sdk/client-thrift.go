package sdk

import (
	"context"
	"encoding/json"
	"math/rand"
	"net"
	"strconv"
	"sync"
	"time"

	"github.com/apache/thrift/lib/go/thrift"
	"gitlab.ghn.vn/common-projects/go-sdk/sdk/thriftapi"
)

// context
var background = context.Background()

// ThriftClient Client for call Thrift service
type ThriftClient struct {
	adr           string
	timeout       int64
	maxConnection int
	maxRetry      int
	waitToRetry   int
	cons          map[string]*ThriftCon
	lock          *sync.Mutex
	debug         bool
}

// ThriftCon Single connection to APIServer
type ThriftCon struct {
	Client   *thriftapi.APIServiceClient
	socket   *thrift.TTransport
	inUsed   bool
	hasError bool
	lock     *sync.Mutex
	id       string
}

// NewThriftClient Constructor
func NewThriftClient(adr string, timeout int64, maxCon int, maxRetry int, waitToRetry int) *ThriftClient {
	return &ThriftClient{
		adr:           adr,
		timeout:       timeout,
		maxConnection: maxCon,
		maxRetry:      maxRetry,
		waitToRetry:   waitToRetry,
		cons:          make(map[string]*ThriftCon),
		lock:          &sync.Mutex{},
	}
}

// SetDebug
func (client *ThriftClient) SetDebug(val bool) {
	client.debug = val
}

// newThriftCon ...
func (client *ThriftClient) newThriftCon() *ThriftCon {
	protocolFactory := thrift.NewTBinaryProtocolFactoryDefault()
	addr, _ := net.ResolveTCPAddr("tcp", client.adr)
	var transport thrift.TTransport
	transport = thrift.NewTSocketFromAddrTimeout(addr, time.Duration(client.timeout)*time.Millisecond)
	transportFactory := thrift.NewTFramedTransportFactory(thrift.NewTBufferedTransportFactory(8192))
	transport, _ = transportFactory.GetTransport(transport)

	iprot := protocolFactory.GetProtocol(transport)
	oprot := protocolFactory.GetProtocol(transport)
	transport.Open()

	return &ThriftCon{
		socket:   &transport,
		Client:   thriftapi.NewAPIServiceClient(thrift.NewTStandardClient(iprot, oprot)),
		inUsed:   false,
		lock:     &sync.Mutex{},
		hasError: false,
	}
}

// pickCon ...
func (client *ThriftClient) pickCon() *ThriftCon {
	for conID, con := range client.cons {
		if con.hasError {
			// remove error connection
			(*con.socket).Close()
			client.cons[conID] = nil
		} else {
			// verify if connection is free
			con.lock.Lock()
			if !con.inUsed {
				con.inUsed = true
				if (*con.socket).IsOpen() {
					con.lock.Unlock()
					return con
				}
				(*con.socket).Close()
				client.cons[conID] = nil
			}
			con.lock.Unlock()
		}
	}

	// if not find any available connection, create new
	con := client.newThriftCon()
	con.inUsed = true

	// append to connection pool if have space
	if client.maxConnection < len(client.cons) {
		id := rand.Intn(999999999) + 1000000000
		client.cons[strconv.Itoa(id)] = con
	}

	return con
}

// call private function to call & retry
func (client *ThriftClient) call(req APIRequest, useNewCon bool) (*thriftapi.APIResponse, error) {

	// map to thrift request
	var r = &thriftapi.APIRequest{
		Path:    req.GetPath(),
		Params:  req.GetParams(),
		Headers: req.GetHeaders(),
		Method:  req.GetMethod().Value,
	}

	if r.Method != "GET" && r.Method != "DELETE" {
		r.Content = req.GetContentText()
	}

	// pick available connection
	var con *ThriftCon
	if !useNewCon {
		con = client.pickCon()
		for con == nil {
			time.Sleep(100 * time.Millisecond)
			con = client.pickCon()
		}
	} else {
		con = client.newThriftCon()
	}
	result, err := con.Client.Call(background, r)

	// verify error
	if err == nil {
		con.lock.Lock()
		con.inUsed = false
		con.lock.Unlock()
	} else {
		con.hasError = true
	}

	return result, err
}

// MakeRequest Call Thrift Service
func (client *ThriftClient) MakeRequest(req APIRequest) *APIResponse {

	canRetry := client.maxRetry
	result, err := client.call(req, false)

	// retry if failed
	for err != nil && canRetry > 0 {
		time.Sleep(time.Duration(client.waitToRetry) * time.Millisecond)
		canRetry--
		result, err = client.call(req, true)
	}

	if err != nil {
		return &APIResponse{
			Status:  APIStatus.Error,
			Message: "Endpoint error: " + err.Error(),
		}
	}

	// parse result
	resp := &APIResponse{
		Status:    result.GetStatus().String(),
		Message:   result.GetMessage(),
		Headers:   result.GetHeaders(),
		Total:     result.GetTotal(),
		ErrorCode: result.GetErrorCode(),
	}
	json.Unmarshal([]byte(result.GetContent()), &resp.Data)
	return resp
}
