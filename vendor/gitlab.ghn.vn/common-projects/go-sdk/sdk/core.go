package sdk

import (
	"errors"
	"fmt"
	"runtime"
	"sync"
	"time"

	"github.com/globalsign/mgo/bson"
)

// Error custom error of sdk
type Error struct {
	Type    string
	Message string
	Data    interface{}
}

func (e *Error) Error() string {
	return e.Type + " : " + e.Message
}

// APIResponse This is  response object with JSON format
type APIResponse struct {
	Status    string            `json:"status"`
	Data      interface{}       `json:"data,omitempty"`
	Message   string            `json:"message"`
	ErrorCode string            `json:"errorCode,omitempty"`
	Total     int64             `json:"total,omitempty"`
	Headers   map[string]string `json:"headers,omitempty"`
}

// StatusEnum ...
type StatusEnum struct {
	Ok           string
	Error        string
	Invalid      string
	NotFound     string
	Forbidden    string
	Existed      string
	Unauthorized string
}

// APIStatus Published enum
var APIStatus = &StatusEnum{
	Ok:           "OK",
	Error:        "ERROR",
	Invalid:      "INVALID",
	NotFound:     "NOT_FOUND",
	Forbidden:    "FORBIDDEN",
	Existed:      "EXISTED",
	Unauthorized: "UNAUTHORIZED",
}

// MethodValue ...
type MethodValue struct {
	Value string
}

// MethodEnum ...
type MethodEnum struct {
	GET     *MethodValue
	POST    *MethodValue
	PUT     *MethodValue
	DELETE  *MethodValue
	OPTIONS *MethodValue
}

// APIMethod Published enum
var APIMethod = MethodEnum{
	GET:     &MethodValue{Value: "GET"},
	POST:    &MethodValue{Value: "POST"},
	PUT:     &MethodValue{Value: "PUT"},
	DELETE:  &MethodValue{Value: "DELETE"},
	OPTIONS: &MethodValue{Value: "OPTIONS"},
}

// BasicInfo Basic stored model
type BasicInfo struct {
	ID              bson.ObjectId `json:"id,omitempty" bson:"_id,omitempty"`
	CreatedTime     time.Time     `json:"createdTime,omitempty"`
	LastUpdatedTime time.Time     `json:"lastUpdatedTime,omitempty"`
}

//App ..
type App struct {
	Name       string
	ServerList []APIServer
	DBList     []*DBClient
	WorkerList []*AppWorker
	launched   bool
}

// NewApp Wrap application
func NewApp(name string) *App {
	app := &App{
		Name:       name,
		ServerList: []APIServer{},
		DBList:     []*DBClient{},
		WorkerList: []*AppWorker{},
		launched:   false,
	}
	return app
}

// SetupDBClient ...
func (app *App) SetupDBClient(config DBConfiguration) *DBClient {
	var db = &DBClient{Config: config}
	app.DBList = append(app.DBList, db)
	return db
}

// SetupAPIServer ...
func (app *App) SetupAPIServer(t string) (APIServer, error) {
	var newID = len(app.ServerList) + 1
	var server APIServer
	switch t {
	case "HTTP":
		server = newHTTPAPIServer(newID)
	case "THRIFT":
		server = newThriftServer(newID)
	}

	if server == nil {
		return nil, errors.New("server type " + t + " is invalid (HTTP/THRIFT)")
	}
	app.ServerList = append(app.ServerList, server)
	return server, nil
}

// SetupWorker ...
func (app *App) SetupWorker() *AppWorker {
	var worker = &AppWorker{}
	app.WorkerList = append(app.WorkerList, worker)
	return worker
}

// callGCManually
func callGCManually() {
	for {
		time.Sleep(2 * time.Minute)
		runtime.GC()
	}
}

// Launch Launch app
func (app *App) Launch() error {

	if app.launched {
		return nil
	}

	app.launched = true
	fmt.Println("[ App " + app.Name + " ] Launching ...")
	var wg = sync.WaitGroup{}

	// start connect to DB
	for _, db := range app.DBList {
		err := db.Connect()
		if err != nil {
			return err
		}
	}

	// start servers
	for _, s := range app.ServerList {
		wg.Add(1)
		go s.Start(&wg)
	}

	// start workers
	for _, wk := range app.WorkerList {
		wg.Add(1)
		go wk.Execute()
	}

	fmt.Println("[ App " + app.Name + " ] Launched!")
	go callGCManually()
	wg.Wait()

	return nil
}
