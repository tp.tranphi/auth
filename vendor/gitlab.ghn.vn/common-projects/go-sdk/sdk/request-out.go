package sdk

import "encoding/json"

// OutboundAPIRequest Request to call other service
type OutboundAPIRequest struct {
	method  string
	path    string
	params  map[string]string
	headers map[string]string
	content string
}

func NewOutboundAPIRequest(method string, path string, params map[string]string, content string, headers map[string]string) APIRequest {
	return &OutboundAPIRequest{
		method:  method,
		path:    path,
		params:  params,
		content: content,
		headers: headers,
	}
}

// GetPath ..
func (req *OutboundAPIRequest) GetPath() string {
	return req.path
}

// GetMethod ..
func (req *OutboundAPIRequest) GetMethod() *MethodValue {
	var s = req.method
	switch s {
	case "GET":
		return APIMethod.GET
	case "POST":
		return APIMethod.POST
	case "PUT":
		return APIMethod.PUT
	case "DELETE":
		return APIMethod.DELETE
	}

	return &MethodValue{Value: s}
}

// GetVar ...
func (req *OutboundAPIRequest) GetVar(name string) string {
	return req.params[name]
}

// GetParam ...
func (req *OutboundAPIRequest) GetParam(name string) string {
	return req.params[name]
}

// GetParams ...
func (req *OutboundAPIRequest) GetParams() map[string]string {
	return req.params
}

// GetContent ...
func (req *OutboundAPIRequest) GetContent(data interface{}) error {
	json.Unmarshal([]byte(req.content), &data)
	return nil
}

// GetContentText ...
func (req *OutboundAPIRequest) GetContentText() string {
	return req.content
}

// GetHeader ...
func (req *OutboundAPIRequest) GetHeader(name string) string {
	return req.headers[name]
}

// GetHeaders ...
func (req *OutboundAPIRequest) GetHeaders() map[string]string {
	return req.headers
}

// GetAttribute ...
func (req *OutboundAPIRequest) GetAttribute(name string) interface{} {
	return nil
}

// SetAttribute ...
func (req *OutboundAPIRequest) SetAttribute(name string, value interface{}) {

}

// GetAttr ...
func (req *OutboundAPIRequest) GetAttr(name string) interface{} {
	return nil
}

// SetAttr ...
func (req *OutboundAPIRequest) SetAttr(name string, value interface{}) {

}
